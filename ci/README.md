This files in this directory have been moved to https://unfurl.cloud/onecommons/ci/ci. If you still have a dashboard or testbed project that is still using these files for its CI pipeline, please update it by visiting your project's `Settings | CI` page and enter `ci/dashboard-gitlab-ci.yml@onecommons/ci` in the "CI/CD configuration file" setting.


