dsl_definitions:
  mock_with_console: &mock_with_console
    Mock:
      operations:
        configure:
          implementation: Template
          inputs:
            dryrun: true
            resultTemplate:
              attributes:
                console_url: https://console.aws.amazon.com/route53/v2/hostedzones

node_types:
  Route53DNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Amazon Web Services Route53 DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Amazon Web Services DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-route53.svg
    attributes:
      console_url:
        type: string
        default: https://console.aws.amazon.com/route53/v2/hostedzones
    properties:
      access_key_id:
        type: string
        title: Access Key ID
        default:
          get_env: AWS_ACCESS_KEY_ID
        required: false
        metadata:
          user_settable: true
      secret_access_key:
        title: Secret Access Key
        type: string
        required: false
        default:
          get_env: AWS_SECRET_ACCESS_KEY
        metadata:
          sensitive: true
          user_settable: true
      session_token:
        title: AWS session token
        type: string
        required: false
        default:
          get_env: AWS_SESSION_TOKEN
        metadata:
          sensitive: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.route53.Route53Provider
          access_key_id:
            eval: access_key_id
          secret_access_key:
            eval: secret_access_key
          session_token:
            eval: session_token
    interfaces: *mock_with_console

  DigitalOceanDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Digitial Ocean DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: DigitalOcean DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/digital_ocean.svg
    attributes:
      console_url:
        type: string
        default: https://cloud.digitalocean.com/networking/domains
    properties:
      DIGITALOCEAN_TOKEN:
        title: DigitalOcean Personal Access Token
        type: unfurl.datatypes.EnvVar
        description: An API token for your DigitalOcean account.
        default:
          get_env: DIGITALOCEAN_TOKEN
        metadata:
          sensitive: true
          user_settable: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.digitalocean.DigitalOceanProvider
          token:
            eval: DIGITALOCEAN_TOKEN

  GoogleCloudDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Google Cloud DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Google Cloud DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/google-cloud-dns.png
    attributes:
      console_url:
        type: string
        default: https://console.cloud.google.com/net-services/dns/zones?project={{ SELF.provider.project }}
        metadata:
          computed: true
          export: true
    properties:
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.googlecloud.GoogleCloudProvider
          credentials_file:
            get_env: GOOGLE_APPLICATION_CREDENTIALS
          project:
            get_env: CLOUDSDK_CORE_PROJECT
    interfaces:
      << : *mock_with_console
      defaults:
        implementation:
          className: unfurl.configurators.dns.DNSConfigurator
          dependencies:
            - google-cloud-dns

  AzureDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Azure DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Azure DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/azure.svg
    properties:
      client_id:
        type: string
        title: Client Id
        description: The Azure Active Directory Application ID (aka client ID)
        default:
          get_env: AZURE_CLIENT_ID
      secret_key:
        type: string
        description: Authentication Key Value (secret)
        metadata:
          sensitive: true
        default:
          get_env: AZURE_SECRET
      directory_id:
        type: string
        title: Directory ID
        description: Directory ID (aka Tenant ID)
        default:
          get_env: AZURE_TENANT
        required: false
      subscription_id:
        type: string
        title: Subscription ID
        required: false
        default:
          get_env: AZURE_SUBSCRIPTION_ID
      resource_group:
        type: string
        title: Resource Group
        required: false
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.azure.AzureProvider
          client_id:
            eval: client_id
          directory_id:
            eval: directory_id
          key:
            eval: secret_key
          sub_id:
            eval: subscription_id
          resource_group:
            eval: resource_group

  CloudFlareDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: CloudFlare DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Cloudflare DNS
    properties:
      cloudflare_token:
        title: Cloudflare Personal Access Token
        type: unfurl.datatypes.EnvVar
        description: An API token for your Cloudflare account.
        default:
          get_env: CLOUDFLARE_TOKEN
        metadata:
          sensitive: true
          user_settable: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.cloudflare.CloudflareProvider
          token:
            eval: cloudflare_token

  RackspaceDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Rackspace DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Rackspace DNS
    properties:
      RACKSPACE_USERNAME: # The the username to authenticate with (required)
        title: Rackspace Username
        type: unfurl.datatypes.EnvVar
        description: Username of your Rackspace account.
        default:
          get_env: RACKSPACE_USERNAME
        metadata:
          user_settable: true
      RACKSPACE_API_KEY: # The api key that grants access for that user (required)
        title: Rackspace Personal Access Token
        type: unfurl.datatypes.EnvVar
        description: An API token for your Rackspace account.
        default:
          get_env: RACKSPACE_API_KEY
        metadata:
          sensitive: true
          user_settable: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.rackspace.RackspaceProvider
          username:
            eval: RACKSPACE_USERNAME
          api_key:
            eval: RACKSPACE_API_KEY

  UnfurlUserDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Default DNS for Unfurl Cloud users
    metadata:
      badge: DNS
      title: Unfurl Cloud DNS
      internal: true
    properties:
      name:
        # note: for security, this isn't actually used by UnfurlUserDNSConfigurator or the unfurl_dns service
        # but it is displayed to the user and referenced by other templates
        description: DNS name of the zone.
        title: Domain name
        type: string
        metadata:
          user_settable: true
          x-read-only: true
          display-value:
            get_env: PROJECT_DNS_ZONE
        default:
            "{{ {'get_ensemble_metadata': 'project_namespace_subdomain'} | eval }}.u.opencloudservices.net"
      provider:
        default: {}
        description: unused
        metadata:
          sensitive: false
      testing:
        metadata:
          user_settable: false
        type: boolean
        default: false
    interfaces:
      defaults:
        implementation:
          className: helpers.py#UnfurlUserDNSConfigurator
      Standard:
        operations:
          delete:
          configure:
      Install:
        operations:
          connect:
      Mock:
        operations:
          configure:
            implementation: Template
            inputs:
              dryrun: true

  UnfurlCNamedDNSZone:
    derived_from: UnfurlUserDNSZone
    description: Unfurl User DNS with a user-provided alias
    properties:
      name:
        description: User's domain for CNAME host
        title: User domain
        type: string
        metadata:
          user_settable: true
        default: ''
      target_subdomain:
        description: The generated subdomain the user's CNAME points to
        title: Target subdomain
        type: string
        metadata:
          user_settable: true
    metadata:
      badge: DNS
      title: Unfurl CNAME DNS
    interfaces:
      Install:
        operations:
          connect: not_implemented
