import logging
import os
import json
from toscaparser.elements.scalarunit import ScalarUnit_Size
import requests
from unfurl.configurator import Configurator
from unfurl.configurators.dns import _get_records

log = logging.getLogger(__file__)


def choose_machine_type(ctx, args):
    """Choose machine type based on memory and cpu"""
    num_cpus = args["num_cpus"]
    mem_size = args["mem_size"]
    mem_size = ScalarUnit_Size(mem_size).get_num_from_scalar_unit("MiB")

    types = args["machine_types"]
    if not os.getenv("UNFURL_MOCK_DEPLOY"):
        types = filter(lambda x: x["mem"] >= mem_size and x["cpu"] >= num_cpus, types)
    types = sorted(types, key=lambda x: (x["cpu"], x["mem"]))

    if types:
        log.info(
            "Selected machine type: %s [CPU: %s, Memory: %s MiB]",
            types[0]["name"],
            types[0]["cpu"],
            types[0]["mem"],
        )
        return types[0]["name"]
    raise ValueError(
        "Can't find satisfactory machine type ({} cpus, {} mem).".format(
            num_cpus, mem_size
        )
    )


def create_variable(ctx, args):
    ctx.task.logger.debug("creating ci variable %s", args.get("key"))
    try:
        return _create_variable(args.pop("key"), args.pop("value"), **args)
    except:
        if ctx.task:
            ctx.task.logger.error("creating variable failed", exc_info=True)
        return None


def _create_variable(
    key, value, variable_type="env_var", masked=False, protected=False
):
    CI_SERVER_URL = os.getenv("CI_SERVER_URL")
    ACCESS_TOKEN = os.getenv("UNFURL_PROJECT_TOKEN")
    CI_PROJECT_ID = os.getenv("CI_PROJECT_ID")
    ENVIRONMENT = os.getenv("CI_ENVIRONMENT_NAME", "*")
    if not (CI_SERVER_URL and ACCESS_TOKEN and CI_PROJECT_ID):
        return None

    if not isinstance(value, str):
        value = json.dumps(value)

    import gitlab

    gitlab = gitlab.Gitlab(CI_SERVER_URL, private_token=ACCESS_TOKEN)
    project = gitlab.projects.get(CI_PROJECT_ID)
    # https://python-gitlab.readthedocs.io/en/stable/api/gitlab.v4.html#gitlab.v4.objects.ProjectVariableManager
    return project.variables.create(
        dict(
            environment_scope=ENVIRONMENT,
            key=key,
            protected=protected,
            masked=masked,
            value=str(value),
            variable_type=variable_type,
        )
    )


class UnfurlUserDNSConfigurator(Configurator):
    DNS_SERVICE_NAME = "project-dns"

    def render(self, task):
        records = _get_records(task.target.attributes)
        for cap in task.target.get_capabilities("resolve"):
            for rel in cap.relationships:
                compute_record = _get_records(rel.attributes)
                assert len(compute_record) == 1
                target_subdomain = task.target.attributes.get("target_subdomain")
                if target_subdomain:
                    value = list(compute_record.values())[0]
                    records.update({target_subdomain: value})
                else:
                    records.update(compute_record)
        return records

    def run(self, task):
        ACCESS_TOKEN = os.getenv("UNFURL_PROJECT_TOKEN")
        assert ACCESS_TOKEN
        # os.getenv('CI_PROJECT_PATH') isn't available locally
        CI_PROJECT_PATH = task.query(
            dict(eval=dict(get_ensemble_metadata="unfurlproject"))
        )
        assert CI_PROJECT_PATH
        UNFURL_SERVICES_URL = (
            os.getenv("UNFURL_SERVICES_URL") or "https://unfurl.cloud/services"
        )
        service_url = f"{UNFURL_SERVICES_URL}/{self.DNS_SERVICE_NAME}"

        action = "DELETE" if task.configSpec.operation == "delete" else "UPSERT"
        records = task.rendered
        ok = True
        for name, record in records.items():
            # see dns-service/app.py
            url = (
                f"{service_url}/update_zone/{name}/{action}/{record['type']}/{record['value']}"
                f"?auth_project={CI_PROJECT_PATH}"
            )
            response = requests.post(url, headers={"PRIVATE-TOKEN": str(ACCESS_TOKEN)})

            if response.status_code != 200:
                ok = False

        yield task.done(success=ok, modified=ok and len(records))
