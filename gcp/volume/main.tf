terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.19"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.19"
    }
  }
}


variable "name" {
  description = "Name of the volume resource"
  type        = string
}
variable "disk_size" {
  description = "Size of the persistent disk, specified in GB."
  type        = number
}
variable "zone" {
  description = "A reference to the zone where the disk resides."
  type        = string
}
variable "labels" {
  type    = map(string)
  default = {}
}


resource "google_compute_disk" "disk" {
  name   = var.name
  size   = var.disk_size
  zone   = var.zone
  labels = var.labels
}


output "disk_id" {
  value = google_compute_disk.disk.id
}

output "disk_zone" {
  value = google_compute_disk.disk.zone
}
