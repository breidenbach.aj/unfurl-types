relationship_types:
  ConnectsTo.DigitalOceanEnvironment:
    derived_from: unfurl.relationships.ConnectsTo.DigitalOcean
    properties:
      credential:
        metadata: {user_settable: false}

node_types:
  unfurl.nodes.DigitalOceanCompute:
    derived_from: unfurl.nodes.Compute
    metadata:
      title: Digital Ocean droplet
    properties:
      key_name:
        title: Key name
        type: string
        description: |
          Name of your Digital Ocean ssh key pair. Set if you want to
          be able to SSH into this instance.
        required: false
      boot_image:
        type: string
        default: ubuntu-22-04-x64
      region:
        type: string
        default: nyc1
      droplet_size:
        type: string
        default:
          eval:
            python: helpers.py#choose_droplet_size
            args:
              num_cpus:
                eval: num_cpus
              mem_size:
                eval: mem_size
    interfaces:
      defaults:
        implementation: Terraform
        inputs:
          tfvars:
            user_data: "{{ SELF.user_data }}"
          main: |
            terraform {
              required_providers {
                digitalocean = {
                  source = "digitalocean/digitalocean"
                  version = "~> 2.0"
                }
              }
            }

            /* Instance ------------------------------- */

            {% if SELF.key_name | default("") %}
            data "digitalocean_ssh_key" "terraform" {
              name = "{{ SELF.key_name }}"
            }
            {% endif %}

            resource "digitalocean_droplet" "web" {
              image = "{{ SELF.boot_image }}"
              name = "{{ SELF.name }}" # XXX check name constraints
              region = "{{ SELF.region }}"  # XXX check if image exist in this region
              size = "{{ SELF.droplet_size }}"

              {% if SELF.user_data %}
              user_data = var.user_data
              {% endif %}

              {% if SELF.key_name | default("") %}
              ssh_keys = [
                data.digitalocean_ssh_key.terraform.id
              ]
              {% endif %}

              # XXX tags
            }

            {%if not SELF.public_ports %}
            data "digitalocean_vpc" "web" {
              region = "{{ SELF.region }}"
            }
            {% endif %}

            /* Firewall ------------------------------- */

            resource "digitalocean_firewall" "web" {
              name = "{{ SELF.name }}-fw"

              droplet_ids = [digitalocean_droplet.web.id]

              {% if SELF.portspecs %}
              {%  for portspec in SELF.portspecs %}
              {%   if portspec and portspec.source and portspec.source not in [80, 22, 443] %}
              inbound_rule {
                protocol         = "{{ portspec.protocol or 'tcp' }}"
                port_range       = "{{ portspec.source }}"
                {%if SELF.public_ports %}
                source_addresses = ["0.0.0.0/0", "::/0"]
                {% else %}
                source_addresses = [data.digitalocean_vpc.web.ip_range]
                {% endif %}
              }
              {%   endif %}
              {%  endfor %}
              {% endif %}

              inbound_rule {
                protocol         = "tcp"
                port_range       = "22"
                source_addresses = ["0.0.0.0/0", "::/0"]
              }

              inbound_rule {
                protocol         = "tcp"
                port_range       = "80"
                {%if SELF.public_ports %}
                source_addresses = ["0.0.0.0/0", "::/0"]
                {% else %}
                source_addresses = [data.digitalocean_vpc.web.ip_range]
                {% endif %}
              }

              inbound_rule {
                protocol         = "tcp"
                port_range       = "443"
                {%if SELF.public_ports %}
                source_addresses = ["0.0.0.0/0", "::/0"]
                {% else %}
                source_addresses = [data.digitalocean_vpc.web.ip_range]
                {% endif %}
              }

              inbound_rule {
                protocol         = "icmp"
                {%if SELF.public_ports %}
                source_addresses = ["0.0.0.0/0", "::/0"]
                {% else %}
                source_addresses = [data.digitalocean_vpc.web.ip_range]
                {% endif %}
              }

              outbound_rule {
                protocol              = "tcp"
                port_range            = "1-65535"
                destination_addresses = ["0.0.0.0/0", "::/0"]
              }

              outbound_rule {
                protocol              = "udp"
                port_range            = "1-65535"
                destination_addresses = ["0.0.0.0/0", "::/0"]
              }
            }

            output "instance" {
              value = digitalocean_droplet.web
            }

            variable "user_data" {
              type        = string
              default     = ""
            }

            variable "tags" {
              type        = map(string)
              default     = {}
            }

      Standard:
        requirements:
          - ConnectsTo.DigitalOceanEnvironment
        operations:
          delete:
          create:
            inputs:
              dryrun_outputs:
                instance:
                  backups: false
                  created_at: "2023-10-06T19:50:57Z"
                  disk: 50
                  droplet_agent: null
                  graceful_shutdown: false
                  id: "378395411"
                  image: "ubuntu-22-04-x64"
                  ipv4_address: "157.230.212.159"
                  ipv4_address_private: "10.116.0.2"
                  ipv6: false
                  ipv6_address: ""
                  locked: false
                  memory: 2048
                  monitoring: false
                  name: "digitaloceandropletJMw"
                  price_hourly: 0.01786
                  price_monthly: 12
                  private_networking: true
                  region: "nyc1"
                  resize_disk: true
                  size: "s-1vcpu-2gb"
                  ssh_keys: null
                  status: "active"
                  tags: null
                  timeouts: null
                  urn: "do:droplet:378395411"
                  user_data: "cae7f3ae39b31a24a402f2bebc78a1a04072d705"
                  vcpus: 1
                  volume_ids: []
                  vpc_uuid: "be5e1e50-5c8b-4089-bc7c-90e7e9d0d0f5"
              resultTemplate:
                attributes:
                  id: "{{ outputs.instance.id }}"
                  urn: "{{ outputs.instance.urn }}"
                  private_address: "{{ outputs.instance.ipv4_address_private }}"
                  public_address: "{{ outputs.instance.ipv4_address }}"
                  # XXX
                  # console_url:
                  #   eval:
                  #     python: helpers.py#get_do_console_url
                  #     args:
                  #       resource_urn: "{{ SELF.urn }}"

      Install:
        requirements:
          - ConnectsTo.DigitalOceanEnvironment
        operations:
          check:
      Mock:
        operations:
          create:
            implementation: Template
            inputs:
              dryrun: true
              resultTemplate:
                attributes: {}
