from unfurl.configurator import Configurator
from functools import cached_property
import requests
import re
import json

def get_azure_vm_console_url(ctx, args):
    """Get Azure console url for a VM"""

    azure_console_format = 'https://portal.azure.com/#@{tenant}/resource/subscriptions/{subscription}/resourceGroups/{resource_group}/providers/Microsoft.Compute/virtualMachines/{instance}/overview'
    return azure_console_format.format(**args)

class AzureAPIHelper:
    def __init__(self, task):
        self.task = task
        self.vm_name = task.target.attributes.get('name', None)

        self.subscription = task.query('$connections::AzureEnvironment::AZURE_SUBSCRIPTION_ID')
        self.tenant = task.query('$connections::AzureEnvironment::AZURE_TENANT')
        self.resource_group = task.query('$connections::AzureEnvironment::resource_group')
        self.client_id = task.query('$connections::AzureEnvironment::AZURE_CLIENT_ID')
        self.client_secret = task.query('$connections::AzureEnvironment::AZURE_SECRET')


    def authorize(self):
        if hasattr(self, 'access_token'):
            return

        url = f'https://login.microsoftonline.com/{self.tenant}/oauth2/token'
        body = f'grant_type=client_credentials&client_id={self.client_id}&client_secret={self.client_secret}&resource=https%3A%2F%2Fmanagement.azure.com%2F'

        oauth_response = requests.post(url, body)
        oauth_response.raise_for_status()
        self.access_token = json.loads(oauth_response.content)['access_token']
        self.task.logger.info(f'Authorized for {self.tenant}')

    @cached_property
    def headers(self):
        self.authorize()
        return {'Authorization': f'Bearer {self.access_token}', 'Content-Type': 'application/json'}

    @cached_property
    def resource_group_record(self):
        url = f'https://management.azure.com/subscriptions/{self.subscription}/resourcegroups/{self.resource_group}?api-version=2021-04-01'
        resource_group_get = requests.get(url, headers=self.headers)
        resource_group_get.raise_for_status()

        return json.loads(resource_group_get.content)

    @property
    def location(self):
        return self.resource_group_record['location']

    def list_vmimage_skus(self, publisher_name, offer):
        url = f'https://management.azure.com/subscriptions/{self.subscription}/providers/Microsoft.Compute/locations/{self.location}/publishers/{publisher_name}/artifacttypes/vmimage/offers/{offer}/skus?api-version=2022-11-01' 
        get_skus = requests.get(url, headers=self.headers)
        get_skus.raise_for_status()

        return json.loads(get_skus.content)

    def restart_vm(self):
        url = f'https://management.azure.com/subscriptions/{self.subscription}/resourceGroups/{self.resource_group}/providers/Microsoft.Compute/virtualMachines/{self.vm_name}/restart?api-version=2022-11-01'
        self.task.logger.debug(f'posting to {url}')
        restart_vm_response = requests.post(url, headers=self.headers)
        restart_vm_response.raise_for_status()

        self.task.logger.info(f'Response from virtual machine resource {restart_vm_response.content}')

    def list_machine_types(self):
        url = f'https://management.azure.com/subscriptions/{self.subscription}/providers/Microsoft.Compute/skus?api-version=2021-07-01&$filter=location eq \'{self.location}\''

        virtual_machine_size_get = requests.get(url, headers=self.headers)
        virtual_machine_size_get.raise_for_status()

        skus = json.loads(virtual_machine_size_get.content)['value']

        # expecting this to get more complicated
        def filter_fn(sku):
            return sku['resourceType'] == 'virtualMachines'

        # don't include the kitchen sink
        def map_fn(sku):
            try:
                result = {}
                for key in ['name', 'tier', 'size']:
                    result[key] = sku[key]

                for key in ['vCPUs', 'MemoryGB', 'CpuArchitectureType']:
                    result[key] = next(capability['value'] for capability in sku['capabilities'] if capability['name'] == key)

                return result
            except:
                return None

        result = skus
        result = filter( filter_fn, result )
        result = map( map_fn, result )

        return result

    @staticmethod
    def all_machine_types(task):
        api_helper = AzureAPIHelper(task)

        machine_types = api_helper.list_machine_types()

        yield from (
            {
                "name": it["name"],
                "mem": float(it["MemoryGB"]) * 1024,
                "cpu": int(it["vCPUs"]),

                # "arch": it["CpuArchitectureType"]
            }
            # TODO support arches
            for it in machine_types if it is not None and it['CpuArchitectureType'] == 'x64'
        )

def all_machine_types(ctx):
    return list(AzureAPIHelper.all_machine_types(ctx.task))


class AzureBootImageConfigurator(Configurator):
    def run(self, task):
        api_helper = AzureAPIHelper(task)

        wanted_sku = task.target.attributes['sku']
        version = task.target.attributes['version']

        skus = api_helper.list_vmimage_skus(
            publisher_name = task.target.attributes['publisher_name'],
            offer = task.target.attributes['offer']
        )

        sku_id = next((sku['id'] for sku in skus if sku['name'] == wanted_sku))

        boot_image = f'{sku_id}/Versions/{version}'

        task.target.attributes['boot_image'] = boot_image

        yield task.done(True)


class AzureVMInstanceConfigurator(Configurator):
    def run(self, task):
        api_helper = AzureAPIHelper(task)

        if task.configSpec.operation == 'restart':
            api_helper.restart_vm()

        yield task.done(True)
