provider "aws" {
  default_tags {
    tags = var.tags
  }
}


variable "instance_id" {
  description = "instance id"
  type        = string
}
variable "volume_id" {
  description = "volume id"
  type        = string
}
variable "device_name" {
  description = "disk device name"
}
variable "persistent" {
  description = "Keep the volume after associated instance is deleted"
  type        = bool
  default     = true
}
variable "tags" {
  type    = map(string)
  default = {}
}


resource "aws_volume_attachment" "attachment" {
  device_name  = var.device_name
  volume_id    = var.volume_id
  instance_id  = var.instance_id
  skip_destroy = var.persistent
}
