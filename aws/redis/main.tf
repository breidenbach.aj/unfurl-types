terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.2"
    }
  }
}

provider "aws" {
    default_tags {
      tags = var.tags
    }
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "app" {
  name = var.name

  vpc_id        = data.aws_vpc.default.id

  ingress {
    description = "custom"
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = [ data.aws_vpc.default.cidr_block ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_elasticache_cluster" "redis" {
  security_group_ids   = [ aws_security_group.app.id ]
  cluster_id           = var.name
  engine               = "redis"
  node_type            = var.machine_type
  availability_zone    = var.availability_zone
  num_cache_nodes      = 1
  parameter_group_name = "default.redis5.0"
  engine_version       = "5.0.6"
  port                 = var.port
}
