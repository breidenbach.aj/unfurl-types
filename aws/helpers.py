import re
from unfurl.configurator import Configurator


def get_ec2_console_url(ctx, args):
    """Get AWS console URL for a given ec2 resource ARN."""
    # arn:aws:ec2:us-west-2:284355325122:instance/i-0fe88f50a1a73b072
    resource_arn_regex = (
        r"^arn:aws:ec2:([A-z]|[0-9]|-)*:[0-9]*:instance/([A-z]|[0-9]|-)*$"
    )
    resource_arn_pattern = re.compile(resource_arn_regex)
    aws_console_format = "https://console.aws.amazon.com/ec2/v2/home?region={region}#InstanceDetails:instanceId={instance_id}"

    resource_arn = args["resource_arn"]

    if not resource_arn_pattern.match(resource_arn):
        raise ValueError(
            f"Invalid ec2 resource ARN, ec2 resource ARN must match {resource_arn_regex}"
        )

    # id format: arn:aws:ec2:{{region}}:{{user}}:instance/{{id}}
    split = resource_arn.split(":")

    parsed = {
        "region": split[3],
        "instance_id": split[5].split("/")[1],
    }

    return aws_console_format.format(**parsed)


def get_awsrds_console_url(ctx, args):
    # arn:aws:rds:us-west-2:362020207168:db:dbinstance
    resource_arn_regex = r"^arn:aws:rds:([A-z]|[0-9]|-)*:[0-9]*:db:([A-z]|[0-9]|-)*$"
    resource_arn_pattern = re.compile(resource_arn_regex)
    aws_console_format = "https://console.aws.amazon.com/rds/home?region={region}#database:id={instance_id};is-cluster=false"

    resource_arn = args["arn"]

    if not resource_arn_pattern.match(resource_arn):
        raise ValueError(
            f"Invalid ec2 resource ARN, ec2 resource ARN must match {resource_arn_regex}"
        )

    # id format: arn:aws:rds:{{region}}:{{number}}:db:{{id}}
    split = resource_arn.split(":")
    parsed = {
        "region": split[3],
        "instance_id": split[6],
    }

    return aws_console_format.format(**parsed)


def get_awselasticache_console_url(ctx, args):
    resource_arn_regex = (
        r"^arn:aws:elasticache:([A-z]|[0-9]|-)*:[0-9]*:cluster:([A-z]|[0-9]|-)*$"
    )
    resource_arn_pattern = re.compile(resource_arn_regex)
    aws_console_format = (
        "https://console.aws.amazon.com/elasticache/home?region={region}#/redis/{name}"
    )

    resource_arn = args["arn"]

    if not resource_arn_pattern.match(resource_arn):
        raise ValueError(
            f"Invalid ec resource ARN, ec resource ARN must match {resource_arn_regex}"
        )

    # id format: arn:aws:elasticache:{{region}}:{{number}}:cluster:{{name}}
    split = resource_arn.split(":")
    parsed = {
        "region": split[3],
        "name": split[6],
    }

    return aws_console_format.format(**parsed)


class EC2InstanceConfigurator(Configurator):
    def run(self, task):
        ec2_instance_id = task.target.attributes["id"]
        success = bool(ec2_instance_id)
        if ec2_instance_id and task.configSpec.operation == "restart":
            response = self.reboot_ec2(task, ec2_instance_id)
            success = response["ResponseMetadata"]["HTTPStatusCode"] == 200
        yield task.done(success)

    def reboot_ec2(self, task, ec2_instance_id):
        import boto3

        endpoint_url = task.query("$connections::AWSAccount::endpoints::ec2")
        client = boto3.client("ec2", endpoint_url=endpoint_url)
        response = client.reboot_instances(
            InstanceIds=[
                ec2_instance_id,
            ],
        )
        return response


class MetadataConfigurator(Configurator):
    def run(self, task):
        task.logger.info("Fetching machine types")
        task.target.attributes["machine_types"] = list(self.all_machine_types(task))
        task.logger.info("Picking availability zone")
        task.target.attributes["availability_zone"] = self.pick_availability_zone(task)
        yield task.done(True)

    def can_dry_run(self, task):
        return True

    @staticmethod
    def all_machine_types(task):
        # XXX add parameter to filter by architecture
        # delay imports until now so the python package can be installed first
        import boto3
        from botocore.exceptions import BotoCoreError

        # for testing with moto, see if there's an endpoints_url set
        endpoint_url = task.query("$connections::AWSAccount::endpoints::ec2")
        client = boto3.client("ec2", endpoint_url=endpoint_url)
        try:
            filters = [
                dict(Name="processor-info.supported-architecture", Values=["x86_64"]),
                dict(Name="supported-virtualization-type", Values=["hvm"]),
            ]
            paginator = client.get_paginator("describe_instance_types")
            for page in paginator.paginate(Filters=filters):
                yield from (
                    {
                        "name": it["InstanceType"],
                        "mem": it["MemoryInfo"]["SizeInMiB"],
                        "cpu": it["VCpuInfo"]["DefaultVCpus"],
                        "arch": it["ProcessorInfo"]["SupportedArchitectures"],
                    }
                    for it in page["InstanceTypes"]
                )
        except BotoCoreError as e:
            task.logger.error("AWS: %s", e)
            raise ValueError("Can't find machine types. Can't communicate with AWS.")

    @staticmethod
    def pick_availability_zone(task):
        # delay imports until now so the python package can be installed first
        import boto3
        from botocore.exceptions import BotoCoreError
        import random

        # for testing with moto, see if there's an endpoints_url set
        endpoint_url = task.query("$connections::AWSAccount::endpoints::ec2")
        client = boto3.client("ec2", endpoint_url=endpoint_url)
        try:
            filters = [
                # find all available AZs in the current region
                dict(Name="state", Values=["available"]),
                dict(Name="zone-type", Values=["availability-zone"]),
                dict(Name="group-name", Values=[client.meta.region_name]),
            ]
            zones = client.describe_availability_zones(Filters=filters)
            return random.choice(zones["AvailabilityZones"])["ZoneName"]

        except BotoCoreError as e:
            task.logger.error("AWS: %s", e)
            raise ValueError(
                "Can't pick availability zone. Can't communicate with AWS."
            )


if __name__ == "__main__":
    print(
        get_ec2_console_url(
            None,
            {
                "resource_arn": "arn:aws:ec2:us-west-2:284355325122:instance/i-0fe88f50a1a73b072"
            },
        )
    )
